require "minitest/autorun"

class FactorialTest < Minitest::Test
  def test_factorial
    assert factorial(0) == 1
    assert factorial(1) == 1
    assert factorial(2) == 2
    assert factorial(3) == 6
    assert factorial(5) == 120
    assert factorial(6) == 720
  end
end

def factorial(n)
  Hash.new { |h, k| h[k] = k > 1 ? k * h[k-1] : 1 }[n]
end

def factorial(n, memo = 1)
  return memo if n == 0

  factorial(n - 1, n * memo)
end

def factorial(n)
  #   (1..n).reduce(:*) || 1
  (1..n).reduce(1, :*)
end
