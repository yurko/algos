require "minitest/autorun"

class ReverseStringTest < Minitest::Test
  def test_reverse
    assert reverse("asd") == "dsa"
    assert reverse("foobar") == "raboof"
  end
end

def reverse(s)
  s.chars.each_with_object([]) { |c, r| r.unshift(c) }.join
end

def reverse(s)
  l = s.length

  (0...l/2).each_with_object(s) { |i| s[i], s[l-1-i] = s[l-1-i], s[i] }
end
