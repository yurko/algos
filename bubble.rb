require "minitest/autorun"

def bubble_sort(arr)
  loop do
    swapped = false
    (arr.size - 1).times do |i|
      if arr[i] > arr[i+1]
        arr[i], arr[i+1] = arr[i+1], arr[i]
        swapped = true
      end
    end
    break unless swapped
  end
  arr
end

class BubbleTest < Minitest::Test
  def test_sort
    assert bubble_sort([4,2,7,0,12,6]) == [0,2,4,6,7,12]
  end
end
