require "minitest/autorun"
# 1 2 3 4 5 6 7
# 0,1,1,2,3,5,8

class FibonacciTest < Minitest::Test
  def test_fibonacci
    assert fibonacci(1) == 0
    assert fibonacci(2) == 1
    assert fibonacci(3) == 1
    assert fibonacci(4) == 2
    assert fibonacci(5) == 3
    assert fibonacci(6) == 5
    assert fibonacci(7) == 8
    assert fibonacci(7, 1) == 13
  end
end

def fibonacci(n, f0 = 0, f1 = 1)
  return f0 if n == 1
  return f1 if n == 2

  fibonacci(n - 1, f1, f0 + f1)
end

# def fibonacci(f1 = 0, f2 = 1, counter)
#   return f1 if counter.zero?
#   fibonacci(f2, f1 + f2, counter - 1)
# end
#
# fibohash = Hash.new { |h, k| h[k] = k < 3 ? 1 : h[k-1] + h[k-2] }
